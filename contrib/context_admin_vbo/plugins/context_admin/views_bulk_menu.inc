<?php
//$Id$

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */

$plugin = array(
  'title' => t('Views Administration'),
  'description' => t('Provides a generic Views Bulk Operations based view that should work for most node type needs.'),
);

function context_admin_vbo_views_bulk_menu_content_form(&$form, &$form_state, $cache = NULL) {
  if (!is_null($form_state['handler_id'])) {
    $default_type = $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_vbo_views_bulk_node_type'] ? $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_vbo_views_bulk_node_type'] : $cache->handlers[$form_state['handler_id']]->conf['context_admin_vbo_views_bulk_node_type'];
    $default_pub = $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_vbo_views_bulk_published'] ? $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_vbo_views_bulk_published'] : $cache->handlers[$form_state['handler_id']]->conf['context_admin_vbo_views_bulk_published'];
    $default_st = $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_vbo_operations_group']['sticky'] ? $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_vbo_operations_gorup']['sticky'] : $cache->handlers[$form_state['handler_id']]->conf['context_admin_vbo_operations_gorup']['sticky'];
    $default_ust = $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_vbo_operations_group']['unsticky'] ? $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_vbo_operations_gorup']['unsticky'] : $cache->handlers[$form_state['handler_id']]->conf['context_admin_vbo_operations_gorup']['unsticky'];
    $default_pro = $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_vbo_operations_group']['promote'] ? $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_vbo_operations_gorup']['promote'] : $cache->handlers[$form_state['handler_id']]->conf['context_admin_vbo_operations_gorup']['promote'];
    $default_upro = $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_vbo_operations_group']['unpromote'] ? $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_vbo_operations_gorup']['unpromote'] : $cache->handlers[$form_state['handler_id']]->conf['context_admin_vbo_operations_gorup']['unpromote'];
  }
  else {
    $default_type = $form_state['page']->new_handler->conf['context_admin_vbo_views_bulk_node_type'];
    $default_pub = $form_state['page']->new_handler->conf['context_admin_vbo_views_bulk_published'];
    $default_st = $form_state['page']->new_handler->conf['context_admin_vbo_operations_group']['sticky'];
    $default_ust = $form_state['page']->new_handler->conf['context_admin_vbo_operations_group']['unsticky'];
    $default_pro = $form_state['page']->new_handler->conf['context_admin_vbo_operations_group']['promote'];
    $default_upro = $form_state['page']->new_handler->conf['context_admin_vbo_operations_group']['unpromote'];
  }
  $types = node_get_types();
  foreach ($types as $key => $type) {
    $options[$key] = $type->name;
  }
  $form['context_admin_vbo_views_bulk_node_type'] = array(
    '#type' => 'radios',
    '#title' => t('Node Type'),
    '#required' => TRUE,
    '#options' => $options,
    '#default_value' => $default_type,
  );
  $form['context_admin_vbo_views_bulk_published'] = array(
    '#type' => 'radios',
    '#title' => t('Published Options'),
    '#required' => TRUE,
    '#options' => array(
      'published' => t('Published'),
      'unpublished' => t('Unpublished'),
      'both' => t('both'),
    ),
    '#default_value' => $default_pub,
  );
  $form['context_admin_vbo_operations_group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bulk Operations Options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['context_admin_vbo_operations_group']['sticky'] = array(
    '#type' => 'checkbox',
    '#title' => t('make sticky available in Bulk Operations dropdown'),
    '#default_value' => $default_st,
  );
  $form['context_admin_vbo_operations_group']['unsticky'] = array(
    '#type' => 'checkbox',
    '#title' => t('make unsticky available in Bulk Operations dropdown'),
    '#default_value' => $default_ust,
  );
  $form['context_admin_vbo_operations_group']['promote'] = array(
    '#type' => 'checkbox',
    '#title' => t('make promote  available in Bulk Operations dropdown'),
    '#default_value' => $default_pro,
  );
  $form['context_admin_vbo_operations_group']['unpromote'] = array(
    '#type' => 'checkbox',
    '#title' => t('make unpromote available in Bulk Operations dropdown'),
    '#default_value' => $default_upro,
  );

  return $form;
}

function context_admin_vbo_views_bulk_menu_content_form_submit(&$form, &$form_state) {
  $cache = context_admin_get_page_cache($form_state['page']->subtask_id);
  if (!is_null($form_state['handler_id'])) {
    $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_vbo_views_bulk_node_type'] = $form_state['values']['context_admin_vbo_views_bulk_node_type'];
    $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_vbo_views_bulk_published'] = $form_state['values']['context_admin_vbo_views_bulk_published'];
    $form_state['page']->handlers[$form_state['handler_id']]->conf['context_admin_vbo_operations_group'] = $form_state['values']['context_admin_vbo_operations_group'];
  }
  else {
    $form_state['page']->new_handler->conf['context_admin_vbo_views_bulk_node_type'] = $form_state['values']['context_admin_vbo_views_bulk_node_type'];
    $form_state['page']->new_handler->conf['context_admin_vbo_views_bulk_published'] = $form_state['values']['context_admin_vbo_views_bulk_published'];
    $form_state['page']->new_handler->conf['context_admin_vbo_operations_group'] = $form_state['values']['context_admin_vbo_operations_group'];
  }
  context_admin_set_page_cache($form_state['page']->subtask_id, $form_state['page']);
  return $form_state;
}

function context_admin_vbo_views_bulk_menu_render_page($handler, $base_contexts, $args, $test = TRUE) {
  return views_embed_view($handler->name);
}

function context_admin_vbo_views_bulk_menu_save($handler, $update) {
  views_invalidate_cache();
}

function context_admin_vbo_views_bulk_menu_delete($handler, $update) {
  views_invalidate_cache();
}
